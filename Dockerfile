FROM python:3.7
WORKDIR ./app
ADD . .
# 安装 requirements.txt 中指定的任何所需软件包
RUN pip install -r requirements.txt

CMD ["python", "./app/greeter_server.py"]
